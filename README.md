# Tarea 5 Laboratorio Software Avanzado


Acontinuacion se encuentran los aspectos generales de la tarea 5 del laboratorio Software Avanzado, en el cual se basa sobre la implementacion de un ESB con 3 microservicios, tomando en cuenta que se desarrollaron puntos como versionamiento SEMVER, ramas basicas como MASTER y DEVELOP por medio de GitLab, incorporando una herramienta de construccion de artefactos como lo es GULP, ademas de otras herramientas como SonarQube.

## Comenzando 🚀

## Estructura Carpetas:
    Para la implementacion se utilizo una distribucion de carpetas las cuales se detallan a continuacion:

    SRC: en esta carpeta estan alamacenados todos los archivos correspondientes al desarrollo del proyecto, todo el codigo fuente.
    DIST: en esta carpeta se encuentra alojado los archivos que van a servir para la distribucion, al momento de realiza un deploy.


## Funcionalidades:
    Para la realizacion de la tarea se solicito implementar los siguentes microservicios los cuales estaran orquestados por medio de un ESB que esta desarrollado desde cero. Los microservicios a simular son los siguientes.
    1. Solicitud de servicio por parte del cliente
    2. Recepción de solicitud y aviso al piloto
    3. Solicitud de ubicación (rastreo) desde la administración del servicio de carros

    Teniendo como base los puntos anteriores se tomaron las siguientes generalidades del proyecto.
## Versionamiento SEMVER:
     Se tomo como base un versionamiento X.Y.Z en el cual X son para cambios de funcionalidades que puedan afectar el funcionamiento, Y son cambios que no afectan la continuidad, y la Z que son parches minimos o actualizacion que no afectan directamente el deploy.

## Ramas Basicas: 
    Se crearon dos ramas en el repositorio Git, las cuales son:
        -   Rama Master: En esta rama se encuentra la version estable, que esta en deploy, esta rama unicamente se actualiza cuando se tiene la certeza que el contenido esta listo para salir.
        -   Rama Develop: En esta rama se desarrollan todas las funcionalidades del proyecto, por lo que estan funcionalidades que no estan completas o estan en fase de pruebas.

## Herramienta Construccion de Artefactos: 
    Para la construccion de artefactos se utilizo la herramienta GULP, la cual permite crear un ZIP con todos los archivos necesarios para el deploy del proyecto, esto por medio de una funcion que permite empaquetarlos.

## Utilitario para revision de Estandares:
     Se implementaron dos formas de revisar el estandar, una por medio de la herramienta SONARQUBE, instalandola en la pc, y evaluando el codigo, que se iba a generando por lo que nos arroga una serie de estadisticas como lo puede ser el numero de bugs, errores de implementacion, codigo repetido. Y se utilizo una herramienta que es en tiempo real la cual es un plugin del IDE de desarrollo VSCODE, el cual es SONARLINT la cual detalla las posibles fallas que se estan realizando y permite cambiarlas por un standar.
    ![SONARQUBE](1.png)
    ![SONARQUBE](2.png)
    ![SONARQUBE](3.png)

    Para la evaluacion del codigo en el repositorio se utilizo la herramienta GITLAB CI, la cual permite la configuracion para las pruebas.
    ![SONARQUBE](4.png)



## Construido con 🛠️

Las herramientas utilizadas para el deploy del proyecto son:

* [VSCode](https://code.visualstudio.com/) - VS-Code
* [Github](https://github.com/) - GitHub
* [SEMVER](https://nodejs.org/es/) - SEMVER
* [GITLAB](https://nodejs.org/es/) - GITLAB
* [SONARQUBE](https://nodejs.org/es/) - SONARQUBE
* [SONARLIF](https://nodejs.org/es/) - SONARLIF
* [GULP](https://nodejs.org/es/) - GULP
* [GITLAB CI](https://nodejs.org/es/) - GITLAB CI


## Versionado 📌

Utilizo [GitLab](http://semver.org/) para el versionado. Puede revisar el codigo completo en el siguiente [Repositorio](http://semver.org/)

## Autor ✒️


* **Manuel Fuentes 201213580** - *Desarrollo* 
* **Manuel Fuentes 201213580** - *Documentación*
